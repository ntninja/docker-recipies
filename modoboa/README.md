**Note:** This container is automatically built by [*docker-image-rebuilder*](https://gitlab.com/ntninja/docker-image-rebuilder) every night and will receive updates whenever the upstream Modoboa or Alpine packages change. [(Dockerfile)](https://gitlab.com/ntninja/docker-recipies/blob/master/modoboa/Dockerfile)

# Modoboa Container Set

## Recommended Operating Environment

### A DNSSEC validating DNS resolver

It is strongly recommended to install a recursive DNSSEC-validating DNS resolver such as [Unbound](https://nlnetlabs.nl/projects/unbound/) on the host system. The included Postfix configuration will use this to perform DANE-validation of supporting remote servers to improve security of sent messages. In any case having a local DNS resolver with enabled caching will likely speed up mail processing due to the many DNS queries that Postfix tends to emit for every incoming and outgoing message. If you prefer using a non-recursive DNS resolver, good old `dnsmasq` with a DNSSEC-capable upstream and a few lines of configuration will work too.

#### Unbound

Just install the `unbound` package, start the service and set */etc/resolv.conf* to `nameserver 127.0.0.1`. Unbound verifies DNSSEC by default and updates the trust-anchors automatically.

#### dnsmasq (including NetworkManager on many distros)

If you have the choice between dnsmasq and systemd-resolved, the later is recommended for its easier setup and optional DNS-over-TLS support.

 1. Ensure that dnsmasq is configured to use a DNSSEC-capable upstream.  
    This can be checked using `delv`:
   * `delv @<upstream-server> ietf.org` should say “*; fully validated*” as its first output line
 2. Extend the configuration with the options below to set the trust-anchor and enable validation.  
    The location of the configuration file will vary based on how dnsmasq is started:
   * If started as a standalone service create and edit the file `/etc/dnsmasq.d/dnssec`
   * If started as part of NetworkManager create and edit `/etc/NetworkManager/dnsmasq.d/dnssec` instead
 3. If running standalone ensure that */etc/resolv.conf* is set to `nameserver 127.0.0.1`

```
# Enable DNSSec validation and caching
dnssec

# DNSSec Trust-Anchors
#  - Format is: [<class>,]<domain>,<key_tag>,<algorithm>,<digest-type>,<digest>
#  - Source: https://data.iana.org/root-anchors/root-anchors.xml
#  - Note that DNSSec key “Kjqmt7v“ has expired on 2019-01-11 and hence is not included below.
trust-anchor=.,20326,8,2,E06D44B80B8F1D39A95C0B0D7C65D08458E880409BBC683457104237C7F8EC8D
```

#### systemd-resolved (including NetworkManager on some other distros)

  1. As with dnsmasq ensure that systemd-resolved is configure with a DNSSEC-capable upstream.
  2. Enable enforcing DNSSEC in the resolved configuration */etc/systemd/resolved.conf* by adding`DNSSEC=yes`.  
     systemd-resolved ships the latest version of the trust-anchor as part of its source code, so managing it manually should be unnecessary.
  3. You may want to enable DNS-over-TLS as well for extra security, but how to configure that is out of scope for this guide.

## Setup

Download and extract the quickstart example directory:

```sh
wget "https://gitlab.com/ntninja/docker-recipies/raw/master/modoboa/quickstart.zip"
unzip quickstart.zip
mv quickstart modoboa
cd modoboa
```

…most importantly it will contain a [`docker-compose.yml` file](https://gitlab.com/ntninja/docker-recipies/blob/master/modoboa/quickstart/docker-compose.yml). Feel free to adapt the files to your needs!

### Modoboa

Run the following command to deploy a new modoboa instance (only one instance per container is supported):

```shell
docker-compose run modoboa deploy --collectstatic \
         --domain <hostname of your server> --dburl default:<mysql|postgres>://[user:pass@][host:port]/dbname
```

Make sure that you have a PostgreSQL or MySQL database ready and on the same network as your Modoboa services before doing this. Only PostgreSQL has been tested, MySQL should work as well through; SQLite may work but likely has issues.

### Dovecot

Pre-generate the Dovecot TLS Diffie-Hellman parameters using the following command:

```shell
openssl dhparam -out ./dovecot/config/dh.pem 4096
```

### Start and Enjoy

After starting the the composition using `docker-compose up` the Modoboa container will listen for HTTP requests (not uWSGI) on port 8080.

After installation some settings need to be set for Modoboa's core and plugins at *Modoboa* → *Parameters*:

  * Modoboa Core
       - Tab *Management*, section *Mail Boxes*:
           * Handle mailboxes on filesystem: **No** (enabling this will cause weird `sudo` errors)
  * `modoboa-sievefilters`
       - Tab *SIEVE-Filters*, section *ManageSieve-Settings*:
           * Server-Address: **dovecot**
           * Server-Port: **4190**
           * Use StartTLS: **No** (Data will only be sent through Docker's local loopback interfaces)
       - Tab *SIEVE-Filters*, section *IMAP-Settings*:
           * Server-Address: **dovecot**
           * Secure Connection: **No** (Data will only be sent through Docker's local loopback interfaces)
           * Server-Port: **143**
  * `modoboa-stats`
       - All default settings (`/var/log/mail.log` & `/tmp/modoboa`) are implemented by the container – don't change these paths!
  * `modoboa-webmail`
       - Tab *Webmail*, section *IMAP-Settings*:
           * Server-Address: **dovecot**
           * Secure Connection: **No** (Data will only be sent through Docker's local loopback interfaces)
           * Server-Port: **143**
       - Tab *Webmail*, section *SMTP-Settings*:
           * Server-Address: **postfix**
           * Secure Connection: **None** (Data will only be sent through Docker's local loopback interfaces)
           * Server-Port: **587**

 

## Management

All important management scripts should be available using the `docker-compose run modoboa …`:

 * Running the `modoboa-admin.py` script:
    * `docker-compose run modoboa admin <options>`
    * Note that you will likely not need this command, as upgrades and deployments can be done using separate commands instead (see the relevant sections)
 * Running `python3 manage.py` on the deployed instance:
    * `docker-compose run modoboa manage <options>`
 * Installing Modoboa [Python modules/extensions](https://github.com/modoboa/modoboa#main-features):
    * `docker-compose run modoboa install <name>`

## Upgrading

Edit the version number in the `docker-compose.yml` file, then stop your modoboa setup, using `docker-compose down` or similar, and execute the following commands in the modoboa directory:

```sh
# Update image
docker-compose pull

# Update installed Python plugins
docker-compose run modoboa update

# Migrate data and refresh static file directory
docker-compose run modoboa manage migrate
echo yes | docker-compose run modoboa manage collectstatic
```

### Upgrade notes: Modoboa 1.15

  * The Modoboa container has always used Python 3, so the removal of Python 2 support does not affect you.
  * That said, the Python version was updated from 3.7 to 3.8, so apply the following changes to avoid errors during the main upgrade procedure:
     * Rename the installed plugins directory: `mv modoboa/data/packages-prefix/lib/python3.7 modoboa/data/packages-prefix/lib/python3.8`
     * Change the path inside the `STATICFILES_DIRS` in *modoboa/data/instance/instance/settings.py* to start with `/usr/lib/python3.8/` rather then `/usr/lib/python3.7/`
  * As per the Modoboa release notes, perform the following steps after pull the new image:
     * Add `DISABLE_DASHBOARD_EXTERNAL_QUERIES = False` to *modoboa/data/instance/instance/settings.py*
     * (Postfix map files are automatically regenerated on container startup.)

### Upgrade notes: Modoboa 1.14

  * Added note about the *Handle mailboxes on filesystem* setting, since it may not be enabled.
  * LDAP is not tested, would welcome feedback.

### Upgrade notes: Modoboa 1.13

The Postfix container now uses the `ntninja/modoboa-postfix:latest` image (rather then the previous `ntninja/alpine-postfix` image). Please update your compose file to reflect this or the stats plugin will not work.

The “TLS setup” of the Postfix QuickStart file changed to enable opportunistic DNSSEC/DANE validation support when the receiving host supports this. The new configuration section now reads:

```
# TLS server setup
smtpd_use_tls = yes
smtpd_tls_session_cache_database = btree:${data_directory}/smtpd_scache

# TLS client setup using opportunistic DNSSEC/DANE validation when supported by the receiving server
smtp_dns_support_level = dnssec
smtp_tls_security_level = dane
smtp_tls_session_cache_database = btree:${data_directory}/smtp_scache
```

Also, installing a validating DNS resolver, such as [Unbound](https://nlnetlabs.nl/projects/unbound/), on the host is recommended to ensure that DNSSEC validation is effective. As a side-effect this may also speed up Postfix' message DNS checks in general if you previously did not have any caching DNS resolver installed locally.

See also the [Modoboa Upgrade Notes](https://modoboa.readthedocs.io/en/latest/upgrade.html#id3) for other required steps.

## Getting Help / Reporting Issues

While I will not guarantee any kind of timely response, please do open issues with questions and suggestions [on the GitLab repo](https://gitlab.com/ntninja/docker-recipies/issues). Alternatively, you may send me a e-mail to [alexander-ws2@ninetailed.ninja](mailto:alexander-ws2@ninetailed.ninja).
